

import TweenMax from 'gsap';
import lpcYandexMap from './components/yandexMap';



ymaps.ready(function () {
    lpcYandexMap.init({
        id: 'map',
        point: [55.740679, 37.632943],
        center: [55.740679, 37.632943],
        zoom: 18,
        greyscale: true,
        pointLayout: '<div class="lpc-yandex-map__marker"><img src="img/point.svg"></div>'
    });
});




function isIE(){
    if (/MSIE 10/i.test(navigator.userAgent)) {
        // This is internet explorer 10
        return true;
    } else if (/MSIE 9/i.test(navigator.userAgent) || /rv:11.0/i.test(navigator.userAgent)) {
        // This is internet explorer 9 or 11
        return true;
    } else if (/Edge\/\d./i.test(navigator.userAgent)){
        // This is Microsoft Edge
        return true;
    }
}





/* ------------------------------------
            ANIMATIONS
 ------------------------------------- */

if(!isMobile()) {
    TweenMax.set('#about-title', {opacity:0});
    TweenMax.set([".project__cover-name",".project__title",".project__btn"], {opacity:0});
    TweenMax.set([".team-person__name",".team-person__regal"], {opacity:0});
    //TweenMax.set([".partner"], {opacity:0});
    TweenMax.set([".about__text-wrapper p"], {opacity:0});
}

function animTitle() {
    TweenMax.fromTo("#about-title",0.7,{opacity:0, y: 40},{opacity:1, y: 0});
    TweenMax.staggerFromTo(".about__text-wrapper p",0.7,{opacity:0, y: 40},{opacity:1, y: 0, delay:0.2}, 0.3);
    animTitle = null;
}

function animProjects() {
    var elems = [".project__cover-name",".project__title",".project__btn"];
    TweenMax.staggerFromTo(elems, 0.8, {opacity:0, y:40},{opacity:1, y:0}, 0.4);
    animProjects = null;
}

function animTeam() {
    TweenMax.staggerFromTo(".team-person__name", 0.8, {opacity:0, y:40},{opacity:1, y:0}, 0.4);
    TweenMax.staggerFromTo(".team-person__regal", 0.8, {opacity:0, y:40},{opacity:1, y:0}, 0.4);
    animTeam = null;
}
function animPartners() {
    TweenMax.staggerFromTo(".partner", 0.5, {opacity:0, scale: 0.8},{opacity:1, scale:1}, 0.1);
    animPartners = null;
}

function runAnimation(index) {
    if(isMobile()) {
        return false;
    }
    switch (index) {
        case 2:
            setTimeout(animTitle, 600);
            break;
        case 3:
            setTimeout(animProjects, 600);
            break;
        case 5:
            setTimeout(animTeam, 600);
            break;
        case 6:
            //setTimeout(animPartners, 600);
            break;
    }
    return false;
}




/**
 * jQuery validation default settings
 * https://jqueryvalidation.org/jQuery.validator.setDefaults/
 */

jQuery.validator.setDefaults({
    errorClass: "invalid",
    validClass: "success",
    errorElement: "div",
    wrapper: "span",
    rules: {
        name: {
            required: true,
            normalizer: function( value ) {
                return $.trim( value );
            }
        },
        phone: {
            required: true,
            minlength: 10
        },
        email: {
            required: true,
            email:true,
            normalizer: function( value ) {
                return $.trim( value );
            }
        },
        personalAgreement: {
            required: true
        }
    },
    messages: {
        name: {
            required: "Это поле обязательно для заполнения"
        },
        phone: {
            required: 'Это поле обязательно для заполнения',
            minlength: 'Телефон минимум 10 символов'
        },
        email: {
            required: "Это поле обязательно для заполнения",
            email: "Введите правильный email"
        },
        personalAgreement: {
            required: "Согласие на обработку персональных данных обязательно"
        }
    }

});

window.onload = function(){

    // Remove preloader
    $('#preloader').fadeOut(300,function(){$(this).remove();});

    // Render Top Arrow
        var topArrow = new Vivus('top-arr-svg', {
            type: 'delayed',
            duration: 200,
            delay: 100,
            animTimingFunction: Vivus.EASE,
            start: 'manual'
        }, function(current){
            $('.scroll-btn__text').fadeOut(300);
        });

        setTimeout(function(){
            topArrow.play();
        }, 2500);




    if(isMobile()) {
        TweenMax.fromTo('.logo', 0.8,{y:-40}, {opacity: 1, y:0});
    } else {
        var a1 = new Vivus('bg1_anim', {
            duration: 120,
            start: 'autostart',
            animTimingFunction: Vivus.EASE
        }, function(){
            TweenMax.to('.logo', 0.8, {opacity: 1});
            TweenMax.to('#bg1_anim', 1, {opacity: 0.1});
            TweenMax.to('#logo_svg_contur', 0.2, {opacity: 0});
        });
    }
};


$(function(){

    setActiveNav();


    /* ------------------------------------
                NAVIGATION
     ------------------------------------- */
    function toggleNav(){
        $(this).toggleClass('hamburger_active');
        $(".navigation").toggleClass('navigation_active');
    }

    $(".hamburger").click(function(){
        toggleNav();
    });

    $(".navigation li a").click(()=>{
        toggleNav();
    });
    $(".navigation__before").click(()=>{
        toggleNav();
    });

    /* ------------------------------------
                FULL PAGE SCROLL
    ------------------------------------- */
    $('.scroll-btn').click(function(e){
        e.preventDefault();
        $.fn.fullpage.moveSectionDown();
    });
    var fullpageIgnoreSection = [];
    var ignoreTmp = $('.ignore');
    ignoreTmp.map((index,item)=>{
        fullpageIgnoreSection.push($(item).index() + 1);
    });
    $('#fullpage').fullpage({
        verticalCentered: false,
        responsiveWidth: 900,
        scrollOverflow: false,
        //fixedElements: '.header',
        afterResponsive: function(isResponsive){
            if(isResponsive) {
                $('body').addClass('fp-fix');
                fullpageIgnoreSection = 0;
            } else {
                $('body').removeClass('fp-fix');
                fullpageIgnoreSection = $('.ignore').index();
                if(fullpageIgnoreSection !== -1) {
                    fullpageIgnoreSection++;
                }
            }
        },
        afterRender: function(){
        },
        onLeave: function (index, nextIndex, direction) {
            setActiveNav(nextIndex);
            let lEl = fullpageIgnoreSection.length - 1;
            if(direction == 'down' && nextIndex == fullpageIgnoreSection[0]) {
                $.fn.fullpage.moveTo(fullpageIgnoreSection[lEl] + 1);
                return false;
            }
            if(direction == 'up' && nextIndex == fullpageIgnoreSection[lEl]) {
                $.fn.fullpage.moveTo(fullpageIgnoreSection[0] - 1);
                return false;
            }
            if(nextIndex == 4) {
                var el = $('.section').get(nextIndex - 1);
                $(el).removeClass('project-detail_hide');
            }
            console.log("NEXT INDEX",nextIndex);
            runAnimation(nextIndex);
        },
        afterLoad: function(link, index){
        }
    });



    //          P O R T F O L I O       //

    if(isIE()) {
        
    } else {

        $('.showDetailBtn').click(function() {
            var projectNum = $(this).attr('data-index');
            var item = $('.project-detail__item')[projectNum -1];
            $(item).removeClass('project-detail__item_hidden');
            $(item).addClass('project-detail__item_active');
            $('.ignore').removeClass('ignore');
            fullpageIgnoreSection = 0;
            $.fn.fullpage.moveSectionDown();
        });


        $('.project-detail__next-btn').click(function(){
            var active = $(`.project-detail__item_active`);
            var prev = active.prev('.project-detail__item');
            var next = active.next('.project-detail__item');
            if(next.length !== 0) {
                next.removeClass('project-detail__item_hidden');
                active.removeClass('project-detail__item_active');
                setTimeout(()=>{
                    active.addClass('project-detail__item_hidden');
                    next.addClass('project-detail__item_active');
                }, 1200);
            }
            if(next.length == 0) {
                active.addClass('project-detail__item_hidden');
                prev.removeClass('project-detail__item_hidden');
                prev.addClass('project-detail__item_active');
                active.removeClass('project-detail__item_active');
            }
            console.log("NEXT",next);
        });

    }





    $('input[name=phone]').mask('+0 (000) 000-00-00');

    $(".lpc-policy__detail").click(function(e) {
        e.preventDefault();
        $(this).closest('.lpc-policy').find('.lpc-policy__hidden').slideToggle(240);
    });

    $('.showModal').click(() => {
        $.fancybox.open({
            src: "#popup_form",
            type: "inline"
        })
    });

});

/*
 * Валидация формы и отправка данных
 * на сервер при успешной валидации
 */

$('form').submit(function(){
    if($(this).valid()) {
        var form = $(this);
        var link = $(form).attr('action'),
            data = $(form).serialize(),
            name = $(form).find('input[name=name]').val(),
            email = $(form).find('input[name=email]').val(),
            phone = $(form).find('input[name=phone]').val(),
            btnText = $(form).find('button[type=submit]').text();

        $.ajax({
            url: link,
            data: data,
            method: "POST",
            beforeSend: () => {
                $(form)
                    .find('button[type=submit]')
                    .prop("disabled", true)
                    .html('Отправляем...');
            }
        }).done(() => {
            sendTargets();
            alert("success");
            $(form)[0].reset();

        }).fail(() => {
            alert("fail");

        }).always(() => {
            $(form)
                .find('button[type=submit]')
                .prop("disabled", false)
                .text(btnText);
        });
    }
    return false;
});

window.goTo = function(num){
    $.fn.fullpage.moveTo(num);
};


function sendTargets() {
    try {
        //ga('send', 'event', 'target_name', 'Send_Form');
        //yaCounter27354854.reachGoal('target_name');
    } catch (e) {
        console.log(e);
    }
    return false;
}


function isMobile() {
    if(window.innerWidth <= 800) {
        return true;
    } else {
        return false;
    }
}


function setActiveNav(index) {
    $('.navigation ul li a').removeClass('active');
    $('.navigation ul li a').map((i,item)=>{
        var tmp = parseInt($(item).attr('data-index'));
        if(index === tmp) {
            $(item).addClass('active');
            return false;
        }
    });
}
