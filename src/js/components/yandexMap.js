

var lpcYandexMap = {
    init: function (options) {
        var defaultPointLayout = "<div class='lpc-yandex-map__marker'>" +
            "<b>Точка на карте</b>" +
            "<p>Краткое описание данного местоположения</p>" +
            "</div>";

        var id = options.id || "map",
            point = options.point || [0,0],
            center = options.center || [0,0],
            zoom = options.zoom || 10,
            pointTitle = options.pointTitle || "",
            pointLayout = options.pointLayout || defaultPointLayout,
            pointSize = options.pointSize || [[0, 0], [0,0]],
            scroll = options.scroll || false,
            greyscale = options.greyscale || false,
            controls = options.controls || [];

        var map = new ymaps.Map(id, {
            center: center,
            zoom: zoom
            //controls: controls
        });

        var squareLayout = ymaps.templateLayoutFactory.createClass(pointLayout);
        var squarePlacemark = new ymaps.Placemark(
            point, {
                hintContent: pointTitle
            }, {
                iconLayout: squareLayout,
                iconShape: {
                    type: 'Rectangle',
                    coordinates: pointSize
                }
            }
        );


        map.geoObjects.add(squarePlacemark);

        if(!scroll) {
            map.behaviors.disable('scrollZoom');
        }
        if(greyscale) {
            var d = document.getElementById(id);
            d.className += " lpc-yandex-map_theme_greyscale";
        }

    }
};


export default lpcYandexMap;